class Nomad < FPM::Cookery::Recipe
	name 'nomad'
	version lookup('version')
	revision lookup('revision')

	description 'Easily deploy applications at any scale.'
	license 'Mozilla Public License'
	vendor 'HashiCorp'
	homepage 'http://nomadproject.io'

	source "https://releases.hashicorp.com/#{name}/#{version}/#{name}_#{version}_linux_#{lookup('download_arch')}.zip"
	sha256 lookup('sha')

	config_files "/etc/#{name}"
	post_install 'post.sh'
	post_uninstall 'post.sh'

	def build
	end

	def install
		etc(name).install workdir('config.hcl')
		lib('systemd/system').install workdir('systemd.service'), "#{name}.service"
		bin.install name
	end
end
