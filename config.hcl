enable_syslog = true

http_api_response_headers {
	"Access-Control-Allow-Origin" = "*"
	"Access-Control-Max-Age" = "86400"
	"Access-Control-Allow-Headers" = "Content-Type"
	"Access-Control-Allow-Methods" = "GET, POST, PUT, DELETE, OPTIONS"
}

server {
	enabled = true
	bootstrap_expect = 1
}

client {
	enabled = true
	servers = ["127.0.0.1"]

	meta {
	}

	reserved {
		cpu = 100
		memory = 64
		disk = 1024
	}
}

consul {
}

vault {
	enabled = true
	address = "http://127.0.0.1:8200"
	tls_skip_verify = true
	create_from_role = "nomad-cluster"
}
